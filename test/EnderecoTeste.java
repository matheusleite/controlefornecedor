/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import modelo.Endereco;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author User
 */
public class EnderecoTeste {
    
    private Endereco enderecoTeste;
    
    public EnderecoTeste() {
    }
    
    @Before
    public void setUp() throws Exception{
        
    }
    
    @Test
	public void testLogradouro() {
		enderecoTeste = new Endereco();
		enderecoTeste.setLogradouro("QS 04 CONJUNTO 6 CASA 26");
		
		assertEquals(enderecoTeste.getLogradouro(), "QS 04 CONJUNTO 6 CASA 26");
	}
   @Test
	public void testPais() {
		enderecoTeste = new Endereco();
		enderecoTeste.setPais("Brasil");
		
		assertEquals(enderecoTeste.getPais(), "Brasil");
	}
   @Test
	public void testEstado() {
		enderecoTeste = new Endereco();
		enderecoTeste.setEstado("Distrito Federal");
		
		assertEquals(enderecoTeste.getEstado(), "Distrito Federal");
	}
   @Test
	public void testCidade() {
		enderecoTeste = new Endereco();
		enderecoTeste.setCidade("Gama");
		
		assertEquals(enderecoTeste.getCidade(), "Gama");
	}
   @Test
	public void testBairro() {
		enderecoTeste = new Endereco();
		enderecoTeste.setBairro("Setor Leste");
		
		assertEquals(enderecoTeste.getBairro(), "Setor Leste");
	}
   @Test
	public void testComplemento() {
		enderecoTeste = new Endereco();
		enderecoTeste.setComplemento("Esquina da rua dos Bombeiros");
		
		assertEquals(enderecoTeste.getComplemento(), "Esquina da rua dos Bombeiros");
	}
   @Test
	public void testNumero() {
		enderecoTeste = new Endereco();
		enderecoTeste.setNumero("29");
		
		assertEquals(enderecoTeste.getNumero(), "29");
	}
}
