/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import modelo.Produto;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author User
 */
public class ProdutoTeste {
    
    private Produto produtoTeste;
    
    public ProdutoTeste() {
    }
    
    @Before
    public void setUp() throws Exception{
        
    }
    
    @Test
	public void testNome() {
		produtoTeste = new Produto();
		produtoTeste.setNome("Carro");
		
		assertEquals(produtoTeste.getNome(), "Carro");
	}
   @Test
	public void testDescricao() {
		produtoTeste = new Produto();
		produtoTeste.setDescricao("Veículo automotor");
		
		assertEquals(produtoTeste.getDescricao(), "Veículo automotor");
	}
   @Test
	public void testValorCompra() {
		produtoTeste = new Produto();
		produtoTeste.setValorCompra(56.0);
		
		assertEquals(56.0, produtoTeste.getValorCompra(),0);
	}
   @Test
	public void testValorVenda() {
		produtoTeste = new Produto();
		produtoTeste.setValorVenda(30000.0);
		
		assertEquals(30000.0, produtoTeste.getValorVenda(),0);
	}
   @Test
	public void testQuantidadeEstoque() {
		produtoTeste = new Produto();
		produtoTeste.setQuantidadeEstoque(300.0);
		
		assertEquals(300.0, produtoTeste.getQuantidadeEstoque(),0);
	}
    
}
