/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package modelo;

/**
 *
 * @author User
 */
public class PessoaJuridica extends Fornecedor{
    private String cnpj;
    private String razaoSocial;
    
    public PessoaJuridica(){
        super();
        this.cnpj="vazio";
        this.razaoSocial="vazio";
    }

    public String getCnpj() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    public String getRazaoSocial() {
        return razaoSocial;
    }

    public void setRazaoSocial(String razaoSocial) {
        this.razaoSocial = razaoSocial;
    }
}
