/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package modelo;

import java.util.ArrayList;

/**
 *
 * @author User
 */
public class Fornecedor {
    
    private String nome;
    private ArrayList<String> listaTelefones;
    private Endereco endereco;
    private ArrayList<Produto> listaProdutos;
    
    public Fornecedor(){
        this.endereco = new Endereco();
        this.listaProdutos = new ArrayList<Produto>();
        this.listaTelefones = new ArrayList<String>();
    }
    
    public Fornecedor(String nome){
		this.nome = nome;
	}

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
    
    public void adicionarTelefone(String telefone){
        listaTelefones.add(telefone);
    }
    public void adicionarProduto(Produto produto){
        listaProdutos.add(produto);
    }

    public Endereco getEndereco() {
        return endereco;
    }

    public void setEndereco(Endereco endereco) {
        this.endereco = endereco;
    }

    
}
