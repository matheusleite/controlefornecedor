/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package controle;
import controle.ControleFornecedor;
import java.io.*;
import java.util.ArrayList;
import modelo.Endereco;
import modelo.PessoaFisica;
import modelo.PessoaJuridica;
import modelo.Produto;
/**
 *
 * @author User
 */
public class CadastroFornecedor {
    
    private ArrayList<String> telefone;
    private ArrayList<String> endereco;
    
    public static void main(String args[]) throws IOException{
        //burocracia para leitura de teclado
        InputStream entradaSistema = System.in;
        InputStreamReader leitor = new InputStreamReader(entradaSistema);
        BufferedReader leitorEntrada = new BufferedReader(leitor);
        String entradaTeclado;
        char menuOpcao;
        //instanciando objetos do sistema
        ControleFornecedor umControle = new ControleFornecedor();
        PessoaJuridica pessoaJuridica = new PessoaJuridica();
        PessoaFisica umFornecedor = new PessoaFisica();
        Endereco umEndereco = new Endereco();
        Produto produto = new Produto();
        String nomeFornecedor;
        
        
        //implementando menu de interação
        do{
            System.out.println(" -   MENU INICIAL  -");
            System.out.println("1- Adicionar um Fornecedor Pessoa Física\n");
            System.out.println("2- Adicionar um Fornecedor Pessoa Jurídica\n");
            System.out.println("3- Pesquisar um Fornecedor\n");
            System.out.println("4- Remover um Fornecedor\n");
            
            System.out.println("Insira a opcao: ");
            entradaTeclado = leitorEntrada.readLine();
            menuOpcao = entradaTeclado.charAt(0);
            
             switch (menuOpcao) {
                case '1' :
                    
                    //entrando com os dados do usuário
                    System.out.println("\nInsira o nome do fornecedor: \n");
                    entradaTeclado = leitorEntrada.readLine();
                    String nome = entradaTeclado;
                    
                    System.out.println("\nInsira o telefone do fornecedor: \n");
                    entradaTeclado = leitorEntrada.readLine();
                    String telefone = entradaTeclado;
                    
                    System.out.println("\nInsira o CPF do fornecedor: \n");
                    entradaTeclado = leitorEntrada.readLine();
                    String cpf = entradaTeclado;
                    
                    System.out.println("\nInsira o endereco do fornecedor: \n");
                    System.out.println("País: ");
                    entradaTeclado = leitorEntrada.readLine();
                    String pais = entradaTeclado;
                    
                    System.out.println("\nEstado: ");
                    entradaTeclado = leitorEntrada.readLine();
                    String estado = entradaTeclado;
                    
                    System.out.println("\nCidade: ");
                    entradaTeclado = leitorEntrada.readLine();
                    String cidade = entradaTeclado;
                    
                    System.out.println("\nLogradouro: ");
                    entradaTeclado = leitorEntrada.readLine();
                    String logradouro = entradaTeclado;
                    
                    System.out.println("\nBairro: ");
                    entradaTeclado = leitorEntrada.readLine();
                    String bairro = entradaTeclado;
                    
                    System.out.println("\nNúmero: ");
                    entradaTeclado = leitorEntrada.readLine();
                    String numero = entradaTeclado;
                    
                    System.out.println("\nComplemento: ");
                    entradaTeclado = leitorEntrada.readLine();
                    String complemento = entradaTeclado;
                    
                    System.out.println("\nInsira o produto do fornecedor: \n");
                    System.out.println("Nome do Produto: ");
                    entradaTeclado = leitorEntrada.readLine();
                    String nomeProduto = entradaTeclado;
                    
                    System.out.println("\nDescrição: ");
                    entradaTeclado = leitorEntrada.readLine();
                    String descricao = entradaTeclado;
                    
                    System.out.println("\nValor de Compra: ");
                    entradaTeclado = leitorEntrada.readLine();
                    Double valorCompra = Double.parseDouble(entradaTeclado);
                    
                    System.out.println("\nValor de Venda: ");
                    entradaTeclado = leitorEntrada.readLine();
                    Double valorVenda = Double.parseDouble(entradaTeclado);
                    
                    System.out.println("\nQuantidade em estoque: ");
                    entradaTeclado = leitorEntrada.readLine();
                    Double qtdEstoque = Double.parseDouble(entradaTeclado);
                    
                    //setando os dados inseridos pelo usuário
                    produto.setNome(nomeProduto);
                    produto.setDescricao(descricao);
                    produto.setValorCompra(valorCompra);
                    produto.setValorVenda(valorVenda);
                    produto.setQuantidadeEstoque(qtdEstoque);
                    umEndereco.setPais(pais);
                    umEndereco.setEstado(estado);
                    umEndereco.setCidade(cidade);
                    umEndereco.setLogradouro(logradouro);
                    umEndereco.setBairro(bairro);
                    umEndereco.setNumero(numero);
                    umEndereco.setComplemento(complemento);
                    umFornecedor.setNome(nome);
                    umFornecedor.setCpf(cpf);
                    
                    //adicionando Produto e Telefone
                    umFornecedor.adicionarTelefone(telefone);
                    umFornecedor.adicionarProduto(produto);
                    
                    
                    String mensagem = umControle.adicionarPessoaFísica(umFornecedor);
                    System.out.println(mensagem);
                    
                    System.out.println("\nVoltar ao menu inicial?");
                    System.out.println("1- Sim \n 2- Não\n");
                    entradaTeclado = leitorEntrada.readLine();
                    double opcao = Double.parseDouble(entradaTeclado);
                    
                    if(opcao==2.0){
                        menuOpcao = '0';
                    }
                    
                break;    
                case '2':
                    
                    //entrando com os dados do usuário
                    System.out.println("\nInsira o nome do fornecedor: \n");
                    entradaTeclado = leitorEntrada.readLine();
                    nome = entradaTeclado;
                    
                    System.out.println("\nInsira o telefone do fornecedor: \n");
                    entradaTeclado = leitorEntrada.readLine();
                    telefone = entradaTeclado;
                    
                    System.out.println("\nInsira o CNPJ do fornecedor: \n");
                    entradaTeclado = leitorEntrada.readLine();
                    String cnpj = entradaTeclado;
                    
                    System.out.println("\nInsira a Razão Social do fornecedor: \n");
                    entradaTeclado = leitorEntrada.readLine();
                    String razaoSocial = entradaTeclado;
                    
                    System.out.println("\nInsira o endereco do fornecedor: \n");
                    System.out.println("País: ");
                    entradaTeclado = leitorEntrada.readLine();
                    pais = entradaTeclado;
                    
                    System.out.println("\nEstado: ");
                    entradaTeclado = leitorEntrada.readLine();
                    estado = entradaTeclado;
                    
                    System.out.println("\nCidade: ");
                    entradaTeclado = leitorEntrada.readLine();
                    cidade = entradaTeclado;
                    
                    System.out.println("\nLogradouro: ");
                    entradaTeclado = leitorEntrada.readLine();
                    logradouro = entradaTeclado;
                    
                    System.out.println("\nBairro: ");
                    entradaTeclado = leitorEntrada.readLine();
                    bairro = entradaTeclado;
                    
                    System.out.println("\nNúmero: ");
                    entradaTeclado = leitorEntrada.readLine();
                    numero = entradaTeclado;
                    
                    System.out.println("\nComplemento: ");
                    entradaTeclado = leitorEntrada.readLine();
                    complemento = entradaTeclado;
                    
                    System.out.println("\nInsira o produto do fornecedor: \n");
                    System.out.println("Nome do Produto: ");
                    entradaTeclado = leitorEntrada.readLine();
                    nomeProduto = entradaTeclado;
                    
                    System.out.println("\nDescrição: ");
                    entradaTeclado = leitorEntrada.readLine();
                    descricao = entradaTeclado;
                    
                    System.out.println("\nValor de Compra: ");
                    entradaTeclado = leitorEntrada.readLine();
                    valorCompra = Double.parseDouble(entradaTeclado);
                    
                    System.out.println("\nValor de Venda: ");
                    entradaTeclado = leitorEntrada.readLine();
                    valorVenda = Double.parseDouble(entradaTeclado);
                    
                    System.out.println("\nQuantidade em estoque: ");
                    entradaTeclado = leitorEntrada.readLine();
                    qtdEstoque = Double.parseDouble(entradaTeclado);
                    
                    //setando os dados inseridos pelo usuário
                    produto.setNome(nomeProduto);
                    produto.setDescricao(descricao);
                    produto.setValorCompra(valorCompra);
                    produto.setValorVenda(valorVenda);
                    produto.setQuantidadeEstoque(qtdEstoque);
                    umEndereco.setPais(pais);
                    umEndereco.setEstado(estado);
                    umEndereco.setCidade(cidade);
                    umEndereco.setLogradouro(logradouro);
                    umEndereco.setBairro(bairro);
                    umEndereco.setNumero(numero);
                    umEndereco.setComplemento(complemento);
                    pessoaJuridica.setNome(nome);
                    pessoaJuridica.setCnpj(cnpj);
                    pessoaJuridica.setRazaoSocial(razaoSocial);
                    
                    //adicionando Produto e Telefone
                    pessoaJuridica.adicionarTelefone(telefone);
                    pessoaJuridica.adicionarProduto(produto);
                    
                    mensagem = umControle.adicionarPessoaJuridica(pessoaJuridica);
                    System.out.println(mensagem);
                    
                    System.out.println("\nVoltar ao menu inicial?");
                    System.out.println("1- Sim \n 2- Não\n");
                    entradaTeclado = leitorEntrada.readLine();
                    opcao = Double.parseDouble(entradaTeclado);
                    
                    if(opcao==2.0){
                        menuOpcao = '0';
                    }
                break;
                case '3':
                    System.out.println("Digite o nome a ser pesquisado: ");
                    entradaTeclado = leitorEntrada.readLine();
                    nomeFornecedor = entradaTeclado;
                    
                    mensagem = umControle.pesquisarNome(nomeFornecedor);
                    System.out.println(mensagem);
                    
                    System.out.println("\nVoltar ao menu inicial?");
                    System.out.println("1- Sim \n 2- Não\n");
                    entradaTeclado = leitorEntrada.readLine();
                    opcao = Double.parseDouble(entradaTeclado);
                    
                    if(opcao==2.0){
                        menuOpcao = '0';
                    }
                break;
                case '4':
                    System.out.println("Digite o nome a ser removido: ");
                    entradaTeclado = leitorEntrada.readLine();
                    nomeFornecedor = entradaTeclado;
                      
                    mensagem = umControle.removeFornecedor(nomeFornecedor);
                    System.out.println(mensagem);
                    
                    System.out.println("\nVoltar ao menu inicial?");
                    System.out.println("1- Sim \n 2- Não\n");
                    entradaTeclado = leitorEntrada.readLine();
                    opcao = Double.parseDouble(entradaTeclado);
                    
                    if(opcao==2.0){
                        menuOpcao = '0';
                    }
                break;
                default:
                    menuOpcao = '0';
                   
        }
     }while(menuOpcao != '0');
    
        
    
    }  
}
