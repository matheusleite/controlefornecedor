/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package controle;

import java.util.ArrayList;
import modelo.Fornecedor;
import modelo.PessoaFisica;
import modelo.PessoaJuridica;

/**
 *
 * @author User
 */
public class ControleFornecedor {

    private ArrayList<Fornecedor> listaFornecedores;
    
    public ControleFornecedor(){
        this.listaFornecedores = new ArrayList<Fornecedor>();
    }
    
    public String adicionarPessoaFísica(PessoaFisica fornecedor){
        listaFornecedores.add(fornecedor);
        return "Fornecedor Pessoa Física adicionado com sucesso";
    }
    public String adicionarPessoaJuridica(PessoaJuridica fornecedor){
        listaFornecedores.add(fornecedor);
        return "Fornecedor Pessoa Jurídica adicionado com sucesso";
    }
    
    public String pesquisarNome(String umNome) {
        for (Fornecedor umFornecedor: listaFornecedores) {
            if (umFornecedor.getNome().equalsIgnoreCase(umNome)) 
                return "Fornecedor encontrado: " + umFornecedor.getNome();
        }
        return "Fornecedor não encontrado";
    }
    
    public Fornecedor Pesquisa(String umNome) {
        for (Fornecedor umFornecedor: listaFornecedores) {
            if (umFornecedor.getNome().equalsIgnoreCase(umNome)) 
                return umFornecedor;
        }
        return null;
    }
    
   public String removeFornecedor(String umNome){
        for (Fornecedor umFornecedor: listaFornecedores) {
            if (umFornecedor.getNome().equalsIgnoreCase(umNome)){
                listaFornecedores.remove(umFornecedor);
                return "Fornecedor removido com sucesso";
            }
        }
        return "Fornecedor não encontrado";
    }
}
